import Link from "next/link";

type Blog={
    title: string,
    body: string,
    id: number
  }

type BlogsProps={
    blogs: Blog[]
}

const Blogs = ({blogs}: BlogsProps) => {
    return(
        <div className="px-5 pb-8">
         {
           blogs && blogs.length >0 && blogs.map((b, index)=> {
             return(
               <div key={index} className="mt-3">
                  <Link href={`/blogs/${b.id}`}> 
                    <a>
                      <p className="semibold text-2xl">{b.title}</p>
                      <p className="">{b.body}</p>
                      <hr className="mt-3"></hr>
                    </a>
                  </Link>
              </div>
             )
           })
          }
        </div>
    )
}

export default Blogs;