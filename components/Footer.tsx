import Image from "next/image";

const Footer = () => {
    return(
        <div className="bg-black py-4">
                <div className="px-4 mt-4 sm:w-1/3 xl:w-1/6 sm:mx-auto xl:mt-0 xl:ml-auto">
                    <h5 className="text-xl text-white font-bold mb-6 sm:text-center xl:text-center">Stay connected</h5>
                    <div className="flex sm:justify-center xl:justify-center">
                        <a href="#facebook" className="w-8 h-8 border-2 border-gray-400 rounded-full text-center py-1 hover:border-blue-600">
                            <Image src="/fb.svg" width="20" height="20" alt="fb"/>
                        </a>
                        <a href="#twitter" className="w-8 h-8 border-2 border-gray-400 rounded-full text-center py-1 ml-2 hover:border-blue-400">
                            <Image src="/twitter.svg" width="20" height="20" alt="twitter"/>
                        </a>
                        <a href="#youtube" className="w-8 h-8 border-2 border-gray-400 rounded-full text-center py-1 ml-2   hover:border-red-600">
                            <Image src="/yt.svg" width="20" height="20" alt="youtube"/>
                        </a>
                    </div>
                </div>
            </div>
    )
}

export default Footer;