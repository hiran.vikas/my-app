const Hero = () => {
    return(
        <section>
            <div className="min-h-screen bg-cover bg-fixed" 
                style={{backgroundImage: "url(https://images.unsplash.com/photo-1457131760772-7017c6180f05?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0NTg5fQ)"}}>
                <div className="flex h-screen justify-center items-center">
                    <div className="text-center bg-black bg-opacity-50 p-5"> 
                        <h1 className="text-white text-4xl lg:text-6xl">Conscience Technology</h1>
                    </div>
                </div>
            </div>
      </section>
    )
}

export default Hero;