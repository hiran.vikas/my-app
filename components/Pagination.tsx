import { useState } from "react";

type PaginationProps={
    blogsPerPage: number,
    totalBlogs: number,
    paginate: (pageN: number) => void
}

const Pagination = ({blogsPerPage, totalBlogs, paginate}: PaginationProps) => {
    const [selectedPage, setSelectedPage] = useState(1);

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalBlogs / blogsPerPage); i++) {
        pageNumbers.push(i);
    }

    return(
        <nav>
            <ul className='flex justify-center mb-4'>
                {pageNumbers.map((number, index) => (
                <li key={number} className='border'>
                    <a aria-current="page" 
                        onClick={() => {
                            paginate(number);
                            setSelectedPage(number);
                        }}
                        className={`${selectedPage === index+1 ? "px-4 bg-indigo-500 border-indigo-900 text-white hover:bg-gray-50": "px-4 bg-white-500 border-gray-400 text-gray-400"}`}>
                    {number}
                    </a>
                </li>
                ))}
            </ul>
    </nav>
    )
}

export default Pagination;