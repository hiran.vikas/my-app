import gql from "graphql-tag";
import { GetServerSideProps, PreviewData } from "next";
import { ParsedUrlQuery } from "querystring";
import client from "../../components/apollo-client";
import Footer from "../../components/Footer";
import Header from "../../components/Header";

 type Blog={
    __typename: string,
    title: string,
    body: string,
    id: number
  }
type PostProps={
    post: Blog
}
  
const PostDetail = ({post}: PostProps) => {
    console.log("props post: ", post)
    return(
        <div className="flex flex-col h-screen">
            <Header/>
            <div className="flex-grow p-4"> 
                <p className="font-bold text-3xl">{post.title}</p>
                <p className="mt-2">{post.body}</p>
            </div>
            <Footer />
        </div>
    )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    //console.log("wow context: ",  ctx.query);
    const { data } = await client.query({ query: gql`
    {
        post(id: ${ctx.query.post_id}) {
            id
            title
            body
          }
    }
    `})

    console.log("***getPost response: ", data.post);
    return {
        props: { post: data.post }
      } 
}

export default PostDetail;