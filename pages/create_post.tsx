import { useMutation } from "@apollo/client";
import gql from "graphql-tag";
import { FormEventHandler, useState } from "react";
import client from "../components/apollo-client";
import Footer from "../components/Footer";
import Header from "../components/Header";

const CreatePost = () => {
    const [title, setTitle] = useState("");
    const [desc, setDesc] = useState("");

    const onTitleChange = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        setTitle(value);
    }

    const onDescChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
        const value = e.currentTarget.value;
        setDesc(value);
    }

    const shouldDisable = (title && title.length>0 && desc && desc.length>0) ? false : true;


    const createPostApi = async() => {
        await client.mutate({
            mutation: gql`
            mutation createPost($title: String!, $body: String!){
                createPost(input: {title:${title}, body:${desc}}) {
                    id
                }
              }`})
              .then(console.log)
              .catch((e) => { console.log("catch", e) })

    };
    
    return(
        <div className="flex flex-col h-screen">
            <Header />
            <div className="flex-grow"> 
                <form className="w-full px-5 mt-4" onSubmit={(e)=>{
                    e.preventDefault();
                    createPostApi();
                }}>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Title
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" 
                            id="grid-email" 
                            type="text" 
                            placeholder="Title"
                            onChange={onTitleChange}/>
                        {!title && <label className="block uppercase tracking-wide text-red-600 text-xs mb-2">
                        Title can not be empty
                        </label>}
                        </div>
                    </div>
                        
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                 Description
                            </label>
                            <textarea className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                onChange={onDescChange}/>
                            {!desc && <label className="block uppercase tracking-wide text-red-600 text-xs mb-2">
                                Description can not be empty
                            </label>}
                        </div>
                        <div className="flex justify-between w-full px-3 mt-4">
                            <button className={`shadow bg-indigo-600 hover:bg-indigo-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-6 rounded ${shouldDisable? " cursor-not-allowed" : ""}`} 
                                type="submit"
                                disabled={shouldDisable}>
                                Create Post
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <Footer />
    </div>
    )
}

export default CreatePost;