import type { NextPage } from 'next'
import { useEffect, useState } from 'react'
import Blogs from '../components/blogs'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Hero from '../components/Hero'
import Pagination from '../components/Pagination'
import { gql } from "@apollo/client";
import client from '../components/apollo-client'

export type Blog={
  __typename: string,
  title: string,
  body: string,
  id: number
}

type BlogsProps={
  blogs: Blog[]
}

const Home = ({blogs}: BlogsProps) => {
  //const [blogsArray, setBlogsArray] = useState([{id: 1, title: "Vikas hiran", body:"lorem"}, {id: 2, title: "Mahavir Jain", body:"lorem"}, {id: 3, title: "Mustan R", body:"lorem"}, {id: 4, title: "Mustan R1", body:"lorem"}, {id: 5, title: "Mustan R2", body:"lorem"}, {id: 6, title: "Mustan R3", body:"lorem"}, {id: 7, title: "Mustan R4", body:"lorem"},{id: 8, title: "Mustan R5", body:"lorem"}, {id: 9, title: "Mustan R6", body:"lorem"}, {id: 10, title: "Mustan R7", body:"lorem"}]);
  var [blogsArray, setBlogsArray] = useState<Array<Blog>>(blogs);

  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [blogsPerPage, setBlogsPerPage] = useState(6);

  // Get current posts
  const indexOfLastBlog = currentPage * blogsPerPage;
  const indexOfFirstBlog = indexOfLastBlog - blogsPerPage;
  const currentBlogs = blogsArray.slice(indexOfFirstBlog, indexOfLastBlog);

  // Change page
  const paginate = (pageN: number) => setCurrentPage(pageN);

 /*  useEffect(()=>{
    //console.log("Blogs array: ", blogs);
    //setBlogsArray(blogs);
  },[]) */
  return (
    <div >
      <Header />
      <div className="mb-auto">
        <Hero />
        <p className="text-center my-5 text-5xl font-bold">Blogs Section</p>
        <Blogs blogs={currentBlogs}/>
        <Pagination blogsPerPage={blogsPerPage}
          totalBlogs={blogsArray.length}
          paginate={paginate}/>
      </div>
      <Footer />
    </div>
  )
}

export async function getStaticProps() {
  const { data } = await client.query({ query: gql`
  {
    posts{
      data {
        id
        title,
        body
      }
    }
  }
`})
  console.log("****Getting All Posts ");
  //console.log("getAllPosts response: ", data.posts.data);
  return {
    props: { blogs:  data.posts.data},
    revalidate: 1
  }
}

export default Home
